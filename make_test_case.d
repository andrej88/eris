#!/usr/bin/env dub
/+ dub.sdl:
    name "make_test_case"
    version "1.0.0"
    license "public domain"
    dependency "exceeds-expectations" version="~>0.7"
+/

module make_test_case;

import std.conv : to;
import std.file : exists, getcwd, mkdirRecurse, write;
import std.getopt : defaultGetoptPrinter, getopt, GetoptResult;
import std.path : buildPath;
import std.process : Pid, spawnProcess, wait;
import std.stdio : File, stderr, stdout;


int main(string[] args)
{
    bool isError = false;
    bool isRegex = false;

    GetoptResult options = getopt(
    	args,
        "error", "Create the test in the unhappy folder, instead of the happy folder", &isError,
        "regex", "Contents of expected file will be treated as a regular expression", &isRegex,
    );


    if (args.length != 2 || options.helpWanted)
    {
        defaultGetoptPrinter("Usage:\n    " ~ "./make_test_case.d <test case name> [options]", options.options);
        return 1;
    }

    if (!isError && isRegex)
    {
        stderr.writeln("Cannot use --regex flag without --error flag.");
        stderr.writeln("Aborting");
        return 1;
    }

    string testName = args[1];

    string testDirPath = buildPath(
        getcwd(),
        "programs",
        isError ? "unhappy" : "happy",
        testName
    );

    if (exists(testDirPath))
    {
        stderr.writeln("Path already exists:\n    " ~ testDirPath);
        stderr.writeln("Aborting");
        return 1;
    }

    mkdirRecurse(testDirPath);

    string testSourcePath = buildPath(testDirPath, testName ~ ".eris");
    write(testSourcePath, null);

    string testOutputFileName = (
        "expected_" ~
        (isError ? "error" : "output") ~
        (isRegex ? "_match" : "") ~
        ".txt"
    );

    string testOutputPath = buildPath(testDirPath, testOutputFileName);
    write(testOutputPath, null);

    Pid codePid = spawnProcess(["code", testSourcePath, testOutputPath]);
    int codeStatus = wait(codePid);

    if (codeStatus != 0)
    {
        stderr.writeln("Error opening files in code. Received status code " ~ codeStatus.to!string ~ ".");
        return 1;
    }

    return 0;
}
