module eris.interpret;

import eris.ast;
import eris.symbols;
import std.algorithm;
import std.bigint;
import std.conv : to;
import std.range;
import std.variant;


int interpret(Script script)
{
    Interpreter interpreter = new Interpreter();
    return script.accept(interpreter).get!int;
}


private class Interpreter : ASTVisitor
{
    Variant visit(Script script)
    {
        foreach (Statement statement; script.statements)
        {
            statement.accept(this);
        }

        return Variant(0);
    }

    Variant visit(VariableDeclaration declaration)
    {
        declaration.getEnclosingScope().put(declaration.name, declaration.initializer.accept(this));
        return Variant(null);
    }

    Variant visit(FunctionDeclaration functionDeclaration)
    {
        functionDeclaration.getEnclosingScope().put(functionDeclaration.name, functionDeclaration);
        return Variant(null);
    }

    Variant visit(Return returnStatement)
    {
        return (
            returnStatement.expression is null ?
            null.Variant :
            returnStatement.expression.accept(this)
        );
    }

    Variant visit(If ifStatement)
    {
        bool conditionResult = ifStatement.condition.accept(this).get!bool;

        if (conditionResult)
        {
            foreach (Statement statement; ifStatement.statements)
            {
                if (cast(Return) statement) return statement.accept(this);
                statement.accept(this);
            }
        }

        return null.Variant;
    }

    Variant visit(BinaryExpression binaryExpression)
    {
        BigInt lhs = binaryExpression.lhs.accept(this).get!BigInt;
        BigInt rhs = binaryExpression.rhs.accept(this).get!BigInt;

        BigInt result;

        switch (binaryExpression.operatorSymbol)
        {
            case '+': result = lhs + rhs; break;
            case '-': result = lhs - rhs; break;
            case '*': result = lhs * rhs; break;
            case '/': result = lhs / rhs; break;
            default: assert(0, "Unexpected binary operator '" ~ binaryExpression.operatorSymbol ~ "'");
        }

        return result.Variant;
    }

    Variant visit(BoolLiteral boolLiteral)
    {
        return boolLiteral.value.Variant;
    }

    Variant visit(FunctionCall functionCall)
    {
        Expression[] args = functionCall.arguments;
        auto evaluatedArgs = args.map!(arg => arg.accept(this));

        string functionName = (cast(SymbolExpression)(functionCall.functionExpression)).name;

        if (functionName == "writeln")
        {
            import std.stdio : stdout;
            stdout.LockingTextWriter w = stdout.lockingTextWriter();

            assert(args.length <= 1, "writeln was supposed to be called with 0 or 1 arguments, but received more: " ~ args.to!string);

            if (!evaluatedArgs.empty) w.put(evaluatedArgs.front.to!string);

            w.put('\n');
        }
        else
        {
            FunctionDeclaration functionDeclaration = functionCall.getEnclosingScope().get!FunctionDeclaration(functionName);

            foreach (Parameter parameter, Variant evaluatedArg; lockstep(functionDeclaration.func.parameters, evaluatedArgs))
            {
                functionDeclaration.func.innerScope.put(parameter.name, evaluatedArg);
            }

            foreach (Statement statement; functionDeclaration.func.statements)
            {
                if (cast(Return) statement) return statement.accept(this);
                statement.accept(this);
            }
        }

        return Variant(null);
    }

    Variant visit(NumberLiteral numberLiteral) { return Variant(numberLiteral.value.to!BigInt); }
    Variant visit(StringLiteral stringLiteral) { return Variant(stringLiteral.value); }
    Variant visit(SymbolExpression symbolExpression) { return symbolExpression.getEnclosingScope().get(symbolExpression.name); }
}
