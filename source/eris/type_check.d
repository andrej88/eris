module eris.type_check;

import colorize;
import eris.ast;
import eris.type;
import std.algorithm : all;
import std.conv : to;
import std.range : lockstep;
import std.stdio;
import std.variant;


/// Returns true if everything is ok, false if there was an error.
bool checkTypes(Script script)
{
    TypeChecker typechecker = new TypeChecker();
    return script.accept(typechecker).get!bool;
}

class TypeChecker : ASTVisitor
{
    private FunctionDeclaration[] lexicalFunctionStack;
    private void pushFn(FunctionDeclaration fn) { lexicalFunctionStack ~= fn; }
    private void popFn() { lexicalFunctionStack.length -= 1; }
    private FunctionDeclaration currentFn() { return lexicalFunctionStack[$ - 1]; }

    Variant visit(Script script)
    {
        // Put a "synthetic" FunctionDeclaration on the stack, representing the script root.
        pushFn(new FunctionDeclaration(script.getEnclosingScope()));
        bool result = all!(statement => statement.accept(this).get!bool)(script.statements);
        popFn();
        return result.Variant;
    }

    Variant visit(VariableDeclaration declaration)
    {
        declaration.initializer.accept(this);

        Type lhsType = declaration.getEnclosingScope().getType(declaration.name);
        Type rhsType = getExpressionType(declaration.initializer);



        if (lhsType != rhsType)
        {
            stderr.writeln(
                "[ERROR]".color(fg.red, bg.init, mode.bold) ~
                " Variable " ~ declaration.name.color(mode.bold) ~
                " has type " ~ lhsType.name.color(mode.bold) ~
                ", but its initial value has type " ~ rhsType.name.color(mode.bold) ~ "\n" ~
                "      @ " ~ declaration.getSourceLocation().to!string
            );

            return false.Variant;
        }

        return true.Variant;
    }

    Variant visit(FunctionDeclaration functionDeclaration)
    {
        pushFn(functionDeclaration);
        bool isBodyOk = all!(statement => statement.accept(this).get!bool)(functionDeclaration.func.statements);
        popFn();
        return isBodyOk.Variant;
    }

    Variant visit(Return returnStatement)
    {
        Type expressionType = (
            returnStatement.expression is null ?
            new Void() :
            getExpressionType(returnStatement.expression)
        );

        FunctionType currentFnType = cast(FunctionType) currentFn().getEnclosingScope().getType(currentFn().name);
        assert(currentFnType, "Expected " ~ currentFn.name ~ " to have a Function type, but received: " ~ currentFnType.to!string);

        Type currentFnReturnType = currentFnType.returnTypes[0];

        if (expressionType != currentFnReturnType)
        {
            stderr.writeln(
                "[ERROR]".color(fg.red, bg.init, mode.bold) ~
                " Function " ~ currentFn().name.color(mode.bold) ~
                " should return " ~ currentFnReturnType.name.color(mode.bold) ~
                ", but tried to return " ~ expressionType.name.color(mode.bold) ~ "\n" ~
                "      @ " ~ returnStatement.getSourceLocation().to!string
            );

            return false.Variant;
        }

        return true.Variant;
    }

    Variant visit(If ifStatement)
    {
        Type conditionType = getExpressionType(ifStatement.condition);

        bool isBoolean = conditionType == new Bool();

        if (!isBoolean)
        {
            stderr.writeln(
                "[ERROR]".color(fg.red, bg.init, mode.bold) ~
                " The condition of an `if` statement must be " ~ new Bool().name.color(mode.bold) ~
                ", but received " ~ conditionType.name.color(mode.bold) ~ "\n" ~
                "      @ " ~ ifStatement.getSourceLocation().to!string
            );
        }

        return isBoolean.Variant;
    }

    Variant visit(BinaryExpression binaryExpression)
    {
        return true.Variant;
    }

    Variant visit(BoolLiteral boolLiteral)
    {
        return true.Variant;
    }

    Variant visit(FunctionCall functionCall)
    {
        foreach (arg; functionCall.arguments)
        {
            arg.accept(this);
        }

        string functionName = (cast(SymbolExpression)(functionCall.functionExpression)).name;

        if (functionName != "writeln")
        {
            FunctionDeclaration functionDeclaration = functionCall.getEnclosingScope().get!FunctionDeclaration(functionName);

            foreach (Expression arg, Parameter param; lockstep(functionCall.arguments, functionDeclaration.func.parameters))
            {
                Type paramType = functionDeclaration.func.innerScope.getType(param.name);

                Type argType = getExpressionType(arg);

                if (argType != paramType)
                {
                    stderr.writeln(
                        "[ERROR]".color(fg.red, bg.init, mode.bold) ~
                        " Parameter " ~ param.name.color(mode.bold) ~ " has type " ~ paramType.name.color(mode.bold) ~ ", " ~
                        "but was passed a value with type " ~ argType.name.color(mode.bold) ~ "\n" ~
                        "      @ " ~ arg.getSourceLocation().to!string
                    );

                    return false.Variant;
                }
            }
        }

        return true.Variant;
    }

    Variant visit(NumberLiteral numberLiteral)
    {
        return true.Variant; // TODO: implement
    }

    Variant visit(StringLiteral stringLiteral)
    {
        return true.Variant; // TODO: implement
    }

    Variant visit(SymbolExpression symbolExpression)
    {
        return true.Variant; // TODO: implement
    }

    // TODO: Remove this once the type checker class becomes an ASG visitor.
    private Type getExpressionType(Expression e)
    {
        if (cast(StringLiteral) e)
        {
            return new String();
        }
        else if (cast(NumberLiteral) e)
        {
            return new Int();
        }
        else if (cast(BoolLiteral) e)
        {
            return new Bool();
        }
        else if (cast(BinaryExpression) e)
        {
            return new Int();
        }
        else if (SymbolExpression se = cast(SymbolExpression) e)
        {
            return currentFn().func.innerScope.getType(se.name);
        }
        else if (FunctionCall fc = cast(FunctionCall) e)
        {
            string functionName = (cast(SymbolExpression) fc.functionExpression).name;
            FunctionType functionType = cast(FunctionType) currentFn().getEnclosingScope().getType(functionName);
            assert(functionType, "Expected " ~ currentFn.name ~ " to have a Function type, but received: " ~ functionType.to!string);
            return functionType.returnTypes[0];
        }

        throw new Exception("Unexpected expression AST node: " ~ e.to!string);
    }
}
