module eris.app;

import eris.ast;
import eris.interpret;
import eris.parsing.parser;
import eris.type_check;
import eris.type_inference;
import pegged.tohtml;
import std.algorithm;
import std.file;
import std.path;
import std.stdio;


private enum string usageString = "Usage:

eris <source file>
";

int main(string[] args)
{
    if (args.length != 2)
    {
        writeln(usageString);
        return -1;
    }

    string sourceFilePath = args[1];
    string source = readText(sourceFilePath);

    version (tracer)
    {
        import pegged.peg : setTraceConditionFunction;

        bool cond(string ruleName, const ref ParseTree p)
        {
            static startTrace = false;
            if (ruleName.startsWith("Eris.Function"))
                startTrace = true;
            return  /* startTrace &&  */
                ruleName.startsWith("Eris") &&
                ruleName != "Eris.Space" &&
                ruleName != "Eris.FIS" &&
                ruleName != "Eris.FS" &&
                ruleName != "Eris.FVS" &&
                ruleName != "Eris.EndOfLine" &&
                ruleName != "Eris.EndOfLineComment"
            ;
        }

        setTraceConditionFunction(&cond);
    }

    ParseTree pt = parse(source);
    toHTML!(Expand.ifMatch)(pt, buildPath(dirName(sourceFilePath), "parse_tree"));
    assert(pt.name == "Eris", "Expected the parse tree's root to be named \"Eris\", but received: " ~ pt.name);
    Script script = new Script(pt.children[0], sourceFilePath);

    inferTypes(script);
    if (!checkTypes(script)) return 1;
    int exitCode = interpret(script);

    // writeln(program.prettyPrint().data);

    return exitCode;
}
