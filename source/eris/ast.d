module eris.ast;

import eris.symbols : Symbols;
import pegged.peg;
import std.algorithm;
import std.conv : to;
import std.range;
import std.variant;


struct SourceLocation
{
    string file;
    int line;
    int column;

    this(string sourceFilePath, ParseTree pt)
    {
        size_t lineNumber = pt.input[0 .. pt.begin].count('\n') + 1;

        size_t charsUntilPreviousNewline = pt.input[0..pt.begin].retro.countUntil('\n');
        bool isFirstLine = charsUntilPreviousNewline == -1;
        size_t colNumber = (
            isFirstLine ?
            pt.begin + 1 :
            charsUntilPreviousNewline + 1
        );

        this.file = sourceFilePath;
        this.line = lineNumber.to!int;
        this.column = colNumber.to!int;
    }

    string toString() const @safe pure nothrow
    {
        return file ~ ":" ~ line.to!string ~ ":" ~ column.to!string;
    }
}

interface AST
{
    Symbols getEnclosingScope();
    Variant accept(ASTVisitor v);
    SourceLocation getSourceLocation();
}

interface ASTVisitor
{
    Variant visit(Script script);

    // Statements
    Variant visit(VariableDeclaration declaration);
    Variant visit(FunctionDeclaration functionDeclaration);
    Variant visit(Return returnStatement);
    Variant visit(If ifStatement);

    // Expressions
    Variant visit(BinaryExpression sumExpression);
    Variant visit(BoolLiteral boolLiteral);
    Variant visit(FunctionCall functionCall);
    Variant visit(NumberLiteral numberLiteral);
    Variant visit(StringLiteral stringLiteral);
    Variant visit(SymbolExpression symbolExpression);
}


/// A list of statements that will be executed in order.
class Script : AST
{
    private Symbols enclosingScope;
    Statement[] statements;
    private SourceLocation sourceLocation;

    this(ParseTree pt, string sourceFilePath)
    in(pt.name == "Eris.Script", "Expected ParseTree named \"Eris.Script\". Received: "~ pt.name)
    {
        this.enclosingScope = new Symbols("$$ ROOT OF SCRIPT $$", null);
        this.sourceLocation = SourceLocation(sourceFilePath, pt);
        this.statements = pt.children.map!(childPt => Statement.fromParseTree(childPt, sourceFilePath, this.enclosingScope)).array;
    }

    override Symbols getEnclosingScope() { return enclosingScope; }
    override Variant accept(ASTVisitor v) { return v.visit(this); }
    override SourceLocation getSourceLocation() { return sourceLocation; }
}

interface Statement : AST
{
    private static Statement fromParseTree(ParseTree pt, string sourceFilePath, Symbols enclosingScope)
    in(pt.name == "Eris.Statement", "Expected ParseTree named \"Eris.Statement\". Received: " ~ pt.name)
    {
        ParseTree childPt = pt.children[0];

        switch (childPt.name)
        {
            case "Eris.VariableDeclaration":    return new VariableDeclaration(childPt, sourceFilePath, enclosingScope);
            case "Eris.FunctionDeclaration":    return new FunctionDeclaration(childPt, sourceFilePath, enclosingScope);
            case "Eris.Return":                 return new Return(childPt, sourceFilePath, enclosingScope);
            case "Eris.If":                     return new If(childPt, sourceFilePath, enclosingScope);
            case "Eris.Expression_Bare":        return Expression.fromParseTree(childPt, sourceFilePath, enclosingScope);
            default:                            assert(false, "No matching branch for Statement child node \"" ~ childPt.name ~ "\".");
        }
    }
}

interface Expression : Statement
{
    private static Expression fromParseTree(ParseTree pt, string sourceFilePath, Symbols enclosingScope)
    {
        switch (pt.name)
        {
            case "Eris.Expression_Bare":
            case "Eris.Expression_Nested":
            case "Eris.PrimaryExpression":
            case "Eris.ParenthesizedExpression":
                return Expression.fromParseTree(pt.children[0], sourceFilePath, enclosingScope);

            case "Eris.SumExpression_Bare":
            case "Eris.SumExpression_Nested":
            case "Eris.ProductExpression_Bare":
            case "Eris.ProductExpression_Nested":
                return BinaryExpression.fromParseTree(pt, sourceFilePath, enclosingScope);

            case "Eris.NumberLiteral":      return new NumberLiteral(pt, sourceFilePath, enclosingScope);
            case "Eris.BoolLiteral":        return new BoolLiteral(pt, sourceFilePath, enclosingScope);
            case "Eris.StringLiteral":      return new StringLiteral(pt, sourceFilePath, enclosingScope);
            case "Eris.SymbolExpression":   return new SymbolExpression(pt, sourceFilePath, enclosingScope);
            case "Eris.FunctionCall":       return new FunctionCall(pt, sourceFilePath, enclosingScope);

            default: assert(0, "Unknown Expression type: " ~ pt.name);
        }
    }
}

class BinaryExpression : Expression
{
    char operatorSymbol;
    private Symbols enclosingScope;
    private SourceLocation sourceLocation;
    Expression lhs;
    Expression rhs;

    this(Symbols enclosingScope, char operatorSymbol)
    {
        this.enclosingScope = enclosingScope;
        this.operatorSymbol = operatorSymbol;
    }

    override Symbols getEnclosingScope() { return enclosingScope; }
    override Variant accept(ASTVisitor v) { return v.visit(this); }
    override SourceLocation getSourceLocation() { return sourceLocation; }

    private static immutable validParseTreeNodes = [
        "Eris.SumExpression_Bare",
        "Eris.SumExpression_Nested",
        "Eris.ProductExpression_Bare",
        "Eris.ProductExpression_Nested",
    ];

    private static Expression fromParseTree(ParseTree pt, string sourceFilePath, Symbols enclosingScope)
    in (validParseTreeNodes.canFind(pt.name), "BinaryExpresion.fromParseTree received unexpected ParseTree node name: " ~ pt.name)
    {
        if (pt.children.length == 1)
        {
            return Expression.fromParseTree(pt.children[0], sourceFilePath, enclosingScope);
        }

        assert(pt.children.length == 3, "Expected " ~ pt.name ~ " to have 1 or 3 children, received: " ~ pt.children.length.to!string);

        char operator = pt.children[1].matches[0][0];

        BinaryExpression binaryExpression = new BinaryExpression(enclosingScope, operator);

        binaryExpression.lhs = Expression.fromParseTree(pt.children[0], sourceFilePath, enclosingScope);
        binaryExpression.rhs = Expression.fromParseTree(pt.children[2], sourceFilePath, enclosingScope);

        return binaryExpression;
    }
}

/// A variable declaration consisting of a variable name and expression that
/// gets evaluated when the variable is initialized.
class VariableDeclaration : Statement
{
    private Symbols enclosingScope;
    string name;
    string typeName;
    Expression initializer;
    private SourceLocation sourceLocation;

    this(ParseTree pt, string sourceFilePath, Symbols enclosingScope)
    in(pt.name == "Eris.VariableDeclaration", "Expected ParseTree named \"Eris.VariableDeclaration\". Received: " ~ pt.name)
    {
        ParseTree childPt = pt.children[0];

        this.enclosingScope = enclosingScope;
        this.sourceLocation = SourceLocation(sourceFilePath, pt);

        switch (childPt.name)
        {
            case "Eris.TypedVariableDeclaration":
                this.name = childPt.matches[1];
                this.typeName = childPt.matches[3];
                this.initializer = Expression.fromParseTree(childPt.children[2], sourceFilePath, enclosingScope);
                break;

            default:
                assert(false, "No matching branch for VariableDeclaration child node \"" ~ childPt.name ~ "\".");
        }
    }

    override Symbols getEnclosingScope() { return enclosingScope; }
    override Variant accept(ASTVisitor v) { return v.visit(this); }
    override SourceLocation getSourceLocation() { return sourceLocation; }
}

/// A function declaration consisting of a function name and a list of
/// parameters.
class FunctionDeclaration : Statement
{
    Function func;

    private Symbols enclosingScope;
    string name;

    string returnTypeName;
    private SourceLocation sourceLocation;

    // TODO: Remove this when there's an ASG, or the type-checker is improved enough.
    this(Symbols enclosingScope)
    {
        this.enclosingScope = enclosingScope;
        this.name = "$$ ROOT OF SCRIPT $$";
        this.returnTypeName = "Int";
        this.func = new Function(this.name, [], this.enclosingScope);
    }

    this(ParseTree pt, string sourceFilePath, Symbols enclosingScope)
    in(pt.name == "Eris.FunctionDeclaration", "Expected ParseTree named \"Eris.FunctionDeclaration\". Received: " ~ pt.name)
    {
        this.enclosingScope = enclosingScope;
        this.name = pt.matches[1];

        ParseTree parametersPt = pt.children[1];
        this.func = new Function(
            name,
            parametersPt.children.map!(paramPt => Parameter(paramPt.matches[0], paramPt.matches[2])).array,
            enclosingScope
        );

        ParseTree functionReturnTypePt = pt.children[2];
        this.returnTypeName = (
            functionReturnTypePt.matches.length == 2 ?
            functionReturnTypePt.matches[1] :
            "Void"
        );

        ParseTree blockPt = pt.children[3];
        this.func.statements =
            blockPt
            .children
            .map!(statementPt => Statement.fromParseTree(statementPt, sourceFilePath, this.func.innerScope))
            .array;
    }

    override Symbols getEnclosingScope() { return enclosingScope; }
    override Variant accept(ASTVisitor v) { return v.visit(this); }
    override SourceLocation getSourceLocation() { return sourceLocation; }
}

class Return : Statement
{
    private Symbols enclosingScope;
    private SourceLocation sourceLocation;
    Expression expression;
    string functionName;

    this(ParseTree pt, string sourceFilePath, Symbols enclosingScope)
    in(pt.name == "Eris.Return", "Expected ParseTree named \"Eris.Return\". Received: " ~ pt.name)
    {
        this.enclosingScope = enclosingScope;

        ParseTree childPt = pt.children[0];

        if (childPt.name == "Eris.ReturnValue")
        {
            this.expression = Expression.fromParseTree(childPt.children[0], sourceFilePath, enclosingScope);
        }

        this.sourceLocation = SourceLocation(sourceFilePath, childPt);
    }

    override Variant accept(ASTVisitor v) { return v.visit(this); }
    override Symbols getEnclosingScope() { return enclosingScope; }
    override SourceLocation getSourceLocation() { return sourceLocation; }
}

class If : Statement
{
    private Symbols enclosingScope;
    private SourceLocation sourceLocation;

    Expression condition;
    Statement[] statements;

    this(ParseTree pt, string sourceFilePath, Symbols enclosingScope)
    {
        this.enclosingScope = enclosingScope;
        this.condition = Expression.fromParseTree(pt.children[0], sourceFilePath, enclosingScope);

        ParseTree blockPt = pt.children[1];
        this.statements = blockPt.children.map!(childPt => Statement.fromParseTree(childPt, sourceFilePath, this.enclosingScope)).array;

        this.sourceLocation = SourceLocation(sourceFilePath, pt);
    }

    override Variant accept(ASTVisitor v) { return v.visit(this); }
    override Symbols getEnclosingScope() { return enclosingScope; }
    override SourceLocation getSourceLocation() { return sourceLocation; }
}

/// A function call consisting of an expression that evaluates to the function
/// being called, and a list of expressions that each evaluate to an argument.
class FunctionCall : Expression
{
    private Symbols enclosingScope;
    SymbolExpression functionExpression;
    Expression[] arguments;
    private SourceLocation sourceLocation;

    this(ParseTree pt, string sourceFilePath, Symbols enclosingScope)
    in(pt.name == "Eris.FunctionCall", "Expected ParseTree named \"Eris.FunctionCall\". Received: " ~ pt.name)
    {
        this.enclosingScope = enclosingScope;
        this.functionExpression = new SymbolExpression(pt.children[0], sourceFilePath, enclosingScope);

        ParseTree argsPt = pt.children[1];
        assert(argsPt.name == "Eris.FunctionArgs", "Expected FunctionCall's children[1] to be named \"Eris.FunctionArgs\", but received: " ~ argsPt.name);
        this.arguments = argsPt.children.map!(argPt => Expression.fromParseTree(argPt, sourceFilePath, enclosingScope)).array;
    }

    override Symbols getEnclosingScope() { return enclosingScope; }
    override Variant accept(ASTVisitor v) { return v.visit(this); }
    override SourceLocation getSourceLocation() { return sourceLocation; }
}

/// A boolean literal
class BoolLiteral : Expression
{
    private Symbols enclosingScope;
    bool value;
    private SourceLocation sourceLocation;

    this(ParseTree pt, string sourceFilePath, Symbols enclosingScope)
    in(pt.name == "Eris.BoolLiteral", "Expected ParseTree named \"Eris.BoolLiteral\". Received: " ~ pt.name)
    {
        this.enclosingScope = enclosingScope;
        this.value = pt.matches[0].to!bool;
    }

    override Symbols getEnclosingScope() { return enclosingScope; }
    override Variant accept(ASTVisitor v) { return v.visit(this); }
    override SourceLocation getSourceLocation() { return sourceLocation; }
}

/// Any kind of number (could be an integer, a real number, or a special
/// floating-point value).
class NumberLiteral : Expression
{
    private Symbols enclosingScope;
    string value;
    private SourceLocation sourceLocation;

    this(ParseTree pt, string sourceFilePath, Symbols enclosingScope)
    in(pt.name == "Eris.NumberLiteral", "Expected ParseTree named \"Eris.NumberLiteral\". Received: " ~ pt.name)
    {
        this.enclosingScope = enclosingScope;
        this.value = pt.matches[0];
    }

    override Symbols getEnclosingScope() { return enclosingScope; }
    override Variant accept(ASTVisitor v) { return v.visit(this); }
    override SourceLocation getSourceLocation() { return sourceLocation; }
}

/// Evaluates to the value associated with an identifier, according to the
/// scoping rules. Normal people call this "using a variable".
class SymbolExpression : Expression
{
    private Symbols enclosingScope;
    string name;
    private SourceLocation sourceLocation;

    this(ParseTree pt, string sourceFilePath, Symbols enclosingScope)
    {
        this.enclosingScope = enclosingScope;
        this.name = pt.matches[0];
    }

    override Symbols getEnclosingScope() { return enclosingScope; }
    override Variant accept(ASTVisitor v) { return v.visit(this); }
    override SourceLocation getSourceLocation() { return sourceLocation; }
}

class StringLiteral : Expression
{
    private Symbols enclosingScope;
    string value;
    private SourceLocation sourceLocation;

    this(ParseTree pt, string sourceFilePath, Symbols enclosingScope)
    in(pt.name == "Eris.StringLiteral", "Expected ParseTree named \"Eris.StringLiteral\". Received: " ~ pt.name)
    {
        this.sourceLocation = SourceLocation(sourceFilePath, pt);
        this.enclosingScope = enclosingScope;

        ParseTree childPt = pt.children[0];
        this.value = childPt.input[childPt.begin .. childPt.end];
    }

    override Symbols getEnclosingScope() { return enclosingScope; }
    override Variant accept(ASTVisitor v) { return v.visit(this); }
    override SourceLocation getSourceLocation() { return sourceLocation; }
}

class Function
{
    Parameter[] parameters;
    Statement[] statements;
    Symbols innerScope;

    this(string name, Parameter[] parameters, Symbols enclosingScope)
    {
        this.parameters = parameters;
        this.innerScope = new Symbols(name, enclosingScope);
    }
}

/// A function's formal parameter.
struct Parameter
{
    string name;
    string typeName;

    this(string name, string typeName)
    {
        this.name = name;
        this.typeName = typeName;
    }
}
