module eris.symbols;

import eris.type;
import std.conv;
import std.variant : Variant, VariantException;


version(unittest)
{
    import exceeds_expectations;
}

/// Symbol table
class Symbols
{
    string name;
    private Symbols enclosingScope;
    private Variant[string] symbols;
    private Type[string] symbolTypes;

    this(string name, Symbols enclosingScope)
    {
        this.name = name;
        this.enclosingScope = enclosingScope;
    }

    T get(T = Variant)(string name)
    {
        if (name !in symbols)
        {
            if (enclosingScope !is null)
            {
                return enclosingScope.get!T(name);
            }
            else
            {
                throw new ScopeException("Symbol \"" ~ name ~ "\" is not defined in this scope.");
            }
        }

        try
        {
            static if (is(T == Variant))
                return symbols[name];
            else
                return symbols[name].get!T;
        }
        catch (VariantException e)
        {
            string symbolType = symbols[name].type.toString();
            throw new ScopeException(
                "Symbol " ~ name ~ " is defined in this scope, " ~
                "but it is of type " ~ symbolType ~ " which cannot be " ~
                "implicitly converted to the requested type (" ~ T.stringof ~ ").",
                e
            );
        }
    }

    Type getType(string name)
    {
        if (name !in symbolTypes)
        {
            if (enclosingScope !is null)
            {
                return enclosingScope.getType(name);
            }
            else
            {
                throw new ScopeException(
                    "Symbol \"" ~ name ~ "\" has no type in this scope. " ~
                    "Types in the current scope and its parents:\n" ~
                    printTypes()
                );
            }
        }

        return symbolTypes[name];
    }

    void put(T)(string name, T value)
    {
        symbols[name] = Variant(value);
    }

    void putType(string name, Type type)
    {
        symbolTypes[name] = type;
    }

    string printTypes()
    {
        string result = this.name ~ ": ";
        result ~= symbolTypes.to!string;
        result ~= "\n";
        if (enclosingScope !is null)
        {
            result ~= "\t" ~ enclosingScope.printTypes();
        }
        return result;
    }
}

class ScopeException : Exception
{
    private this(string message, string file = __FILE__, int line = __LINE__, Throwable nextInChain = null)
    {
        super(message, file, line, nextInChain);
    }

    private this(string message, Throwable nextInChain, string file = __FILE__, int line = __LINE__)
    {
        super(message, nextInChain, file, line);
    }
}

@("Nested scopes")
unittest
{
    Symbols outer = new Symbols(null);
    Symbols inner = new Symbols(outer);

    outer.put("x", 3);
    outer.put("y", "test");
    inner.put("y", 2);
    inner.put("z", 1.0f);

    expect(outer.get!int("x")).toEqual(3);
    expect(outer.get!string("y")).toEqual("test");
    expect({ outer.get!int("z"); }).toThrow!ScopeException;

    expect(inner.get!int("x")).toEqual(3);
    expect(inner.get!int("y")).toEqual(2);
    expect(inner.get!float("z")).toEqual(1.0f);

    expect({ inner.get!string("y"); }).toThrow!ScopeException;
}
