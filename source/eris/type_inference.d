module eris.type_inference;

import eris.ast;
import eris.type;
import std.variant;


void inferTypes(Script script)
{
    TypeInferrer typeInferrer = new TypeInferrer();
    script.getEnclosingScope().putType("writeln", new Void());
    script.accept(typeInferrer);
}

class TypeInferrer : ASTVisitor
{
    Variant visit(Script script)
    {
        foreach (Statement s; script.statements) s.accept(this);
        return Variant.init;
    }

    Variant visit(VariableDeclaration declaration)
    {
        declaration.initializer.accept(this);

        Type type;
        switch (declaration.typeName)
        {
            case "Int":    type = new Int(); break;
            case "String": type = new String(); break;
            case "Bool":   type = new Bool(); break;
            default:       throw new Exception("Forbidden declaration type: " ~ declaration.typeName);
        }

        declaration.getEnclosingScope().putType(declaration.name, type);
        return Variant.init;
    }

    Variant visit(FunctionDeclaration functionDeclaration)
    {
        Type[] parameterTypes;

        foreach (Parameter parameter; functionDeclaration.func.parameters)
        {
            Type type;

            switch (parameter.typeName)
            {
                case "Int":    type = new Int(); break;
                case "String": type = new String(); break;
                case "Bool":   type = new Bool(); break;
                default:       throw new Exception("Forbidden parameter type: " ~ parameter.typeName);
            }

            parameterTypes ~= type;
            functionDeclaration.func.innerScope.putType(parameter.name, type);
        }

        foreach (Statement s; functionDeclaration.func.statements)
        {
            s.accept(this);
        }

        Type returnType;

        switch (functionDeclaration.returnTypeName)
        {
            case "Void":   returnType = new Void(); break;
            case "Int":    returnType = new Int(); break;
            case "String": returnType = new String(); break;
            default:       throw new Exception("Forbidden return type: " ~ functionDeclaration.returnTypeName);
        }

        functionDeclaration.getEnclosingScope().putType(functionDeclaration.name, new FunctionType(parameterTypes, [returnType]));
        functionDeclaration.getEnclosingScope().put(functionDeclaration.name, functionDeclaration);

        return Variant.init;
    }

    Variant visit(Return returnStatement)
    {
        return Variant(null); // TODO
    }

    Variant visit(If ifStatement)
    {
        return Variant(null); // TODO
    }

    Variant visit(BinaryExpression binaryExpression)
    {
        return Variant.init; // TODO
    }

    Variant visit(BoolLiteral boolLiteral)
    {
        return Variant.init;
    }

    Variant visit(FunctionCall functionCall)
    {
        // TODO: Check symbol table for the function name once a `Function` type exists.

        foreach(Expression arg; functionCall.arguments)
        {
            arg.accept(this);
        }

        return Variant.init;
    }

    Variant visit(NumberLiteral numberLiteral)
    {
        return Variant.init;
    }

    Variant visit(StringLiteral stringLiteral)
    {
        return Variant.init;
    }

    Variant visit(SymbolExpression symbolExpression)
    {
        return Variant.init;
    }
}
