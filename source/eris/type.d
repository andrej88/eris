module eris.type;

import exceeds_expectations;


interface Type
{
    string name();
}

class Int : Type
{
    string name() { return "Int"; }
    override bool opEquals(Object other) const @nogc @safe pure nothrow { return (cast(Int) other) !is null; }
    override size_t toHash() const @safe nothrow { return typeid(Int).toHash(); }
}

class String : Type
{
    string name() { return "String"; }
    override bool opEquals(Object other) const { return (cast(String) other) !is null; }
    override size_t toHash() const @safe nothrow { return typeid(String).toHash(); }
}

class Bool : Type
{
    string name() { return "Bool"; }
    override bool opEquals(Object other) const { return (cast(Bool) other) !is null; }
    override size_t toHash() const @safe nothrow { return typeid(Bool).toHash(); }
}

class Void : Type
{
    string name() { return "Void"; }
    override bool opEquals(Object other) const { return (cast(Void) other) !is null; }
    override size_t toHash() const @safe nothrow { return typeid(Void).toHash(); }
}

class FunctionType : Type
{
    Type[] parameterTypes;
    Type[] returnTypes;

    this(Type[] parameterTypes, Type[] returnTypes)
    {
        this.parameterTypes = parameterTypes;
        this.returnTypes = returnTypes;
    }

    string name()
    {
        return "Function";
    }

    override bool opEquals(Object other) const
    {
        FunctionType otherFn = cast(FunctionType) other;

        return
            (otherFn !is null) &&
            (otherFn.parameterTypes == this.parameterTypes) &&
            (otherFn.returnTypes == this.returnTypes);
    }

    @("FunctionType.opEquals()")
    unittest
    {
        import exceeds_expectations;

        expect(
            new FunctionType(
                cast(Type[]) [new Int(), new String()],
                cast(Type[]) [new Void()]
            )
        ).toEqual(
            new FunctionType(
                cast(Type[]) [new Int(), new String()],
                cast(Type[]) [new Void()]
            )
        );

        expect(
            new FunctionType(
                cast(Type[]) [new Int(), new String()],
                cast(Type[]) [new Void()]
            )
        ).not.toEqual(
            new FunctionType(
                cast(Type[]) [new Int(), new String()],
                cast(Type[]) [new String()]
            )
        );

        expect(
            new FunctionType(
                cast(Type[]) [new Int(), new String()],
                cast(Type[]) [new Void()]
            )
        ).not.toEqual(
            new FunctionType(
                cast(Type[]) [new String(), new String()],
                cast(Type[]) [new Void()]
            )
        );
    }

    override size_t toHash() const @safe nothrow
    {
        return typeid(FunctionType).toHash();
    }
}
