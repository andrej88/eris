#!/usr/bin/env dub
/+ dub.sdl:
    name "run-tests"
    version "1.0.0"
    license "public domain"
    dependency "colorize" version="~>1.0"
    dependency "exceeds-expectations" version="~>0.7"
    stringImportPaths "."
+/

import colorize;
import exceeds_expectations;
import exceeds_expectations.exceptions;
import std.algorithm : filter, fold, joiner, sort;
import std.array : array;
import std.conv : to;
import std.file : dirEntries, DirEntry, exists, getcwd, isDir, isFile, readText, SpanMode;
import std.path : asRelativePath, baseName, buildPath, setExtension;
import std.process : Pid, pipeProcess, ProcessPipes, spawnProcess, wait;
import std.range : chain;
import std.stdio : fflush, File, stderr, stdout, write, writeln;
import std.string : stripRight;


int main(string[] args)
{
    Pid dubBuildPid = spawnProcess(["dub", "build", "--compiler=dmd"] ~ args[1..$]);
    int buildStatus = wait(dubBuildPid);
    if (buildStatus != 0) return buildStatus;

    string erisPath = getcwd().buildPath("eris");

    auto happyPaths = getcwd().buildPath("programs", "happy").dirEntries(SpanMode.shallow).array.sort!((a, b) => a.name < b.name);
    auto unhappyPaths = getcwd().buildPath("programs", "unhappy").dirEntries(SpanMode.shallow).array.sort!((a, b) => a.name < b.name);

    int passed;
    int skipped;
    int failed;
    int total;

    foreach (DirEntry entry; chain(happyPaths, unhappyPaths).filter!(e => e.isDir))
    {
        total++;

        string testSimpleName = entry.name.baseName;
        string sourceFilePath = entry.name.buildPath(testSimpleName).setExtension(".eris");

        string expectedOutputPath = entry.name.buildPath("expected_output.txt");

        bool errorShouldUseMatch = false;
        string expectedErrorPath = entry.name.buildPath("expected_error.txt");
        if (!exists(expectedErrorPath))
        {
            errorShouldUseMatch = true;
            expectedErrorPath = entry.name.buildPath("expected_error_match.txt");
        }

        string sourceFileRelativePath = sourceFilePath.asRelativePath(getcwd()).to!string;
        string testLabel = sourceFileRelativePath.color(mode.bold);

        write("▶".color(fg.blue) ~ " " ~ testLabel);
        fflush(stdout.getFP());

        bool checksStdOut = exists(expectedOutputPath) && isFile(expectedOutputPath);
        bool checksStdErr = exists(expectedErrorPath) && isFile(expectedErrorPath);

        if (!checksStdOut && !checksStdErr)
        {
            write("\r");
            writeln("⏭".color(fg.yellow) ~ " " ~ testLabel ~ "\n  " ~ "Found neither expected_output nor expected_error.".color(fg.yellow));
            skipped++;
            continue;
        }

        fflush(stdout.getFP);

        ProcessPipes pipes = pipeProcess(
            [erisPath, sourceFileRelativePath]
        );

        int status = wait(pipes.pid);

        if (!checksStdErr && status != 0)
        {
            write("\r");
            writeln("✗".color(fg.red) ~ " " ~ testLabel);
            writeln("Test case failed with exit code " ~ status.to!string ~ ", but no expected_stderr file was found. Program's standard error stream:");
            writeln(readFileText(pipes.stderr));
            writeln();
            failed++;
            continue;
        }


        string failingOutputMessage;
        string failingErrorMessage;

        try
        {
            string actualOutput = removeSgrCodes(readFileText(pipes.stdout));
            string expectedOutput = checksStdOut ? readText(expectedOutputPath).stripRight("\n") : "";
            expect(actualOutput).toEqual(expectedOutput);
        }
        catch (FailingExpectationError e)
        {
            failingOutputMessage = e.message().to!string;
        }

        try
        {
            string actualError = removeSgrCodes(readFileText(pipes.stderr));
            string expectedError = checksStdErr ? readText(expectedErrorPath).stripRight("\n") : "";

            if (errorShouldUseMatch)
                expect(actualError).toMatch(expectedError);
            else
                expect(actualError).toEqual(expectedError);
        }
        catch (FailingExpectationError e)
        {
            failingErrorMessage = e.message.to!string;
        }

        if (failingOutputMessage != string.init || failingErrorMessage != string.init)
        {
            write("\r");
            writeln("✗".color(fg.red) ~ " " ~ testLabel);

            if (failingOutputMessage != string.init)
            {
                writeln("Unexpected standard output:");
                writeln(failingOutputMessage);
            }

            if (failingErrorMessage != string.init)
            {
                writeln("Unexpected standard error:");
                writeln(failingErrorMessage);
            }

            failed++;
        }
        else
        {
            write("\r");
            writeln("✓".color(fg.green) ~ " " ~ testLabel);

            passed++;
        }
    }

    writeln();
    writeln(
        (passed.to!string ~ " passed").color(fg.green) ~ "  " ~
        (failed.to!string ~ " failed").color(fg.red) ~ "  " ~
        (skipped.to!string ~ " skipped").color(fg.yellow) ~ "  " ~
        total.to!string ~ " total"
    );

    return 0;
}

string readFileText(File file)
{
    return file.byLine.joiner("\n").array.to!string;
}

string removeSgrCodes(string input)
{
    import std.regex : regex, replaceAll;

    return replaceAll(input, regex("\033" ~ `\[[\d;]*m`), "");
}
