#!/usr/bin/env dub
/+ dub.sdl:
    name "test-watch"
    version "1.0.0"
    license "public domain"
    dependency "fswatch" version="~>0.6"
+/

import core.thread;
import fswatch;
import std.algorithm;
import std.datetime;
import std.process;
import std.string;


void main(string[] args)
{
    FileWatch sourceWatcher = FileWatch("source/", true);
    FileWatch programsWatcher = FileWatch("programs/", true);

    runTests(args);

    while (true)
    {
        FileChangeEvent[] sourceChanges = sourceWatcher.getEvents();
        FileChangeEvent[] programsChanges = programsWatcher.getEvents();

        if (
            sourceChanges.length > 0 ||
            (programsChanges.length > 0 && programsChanges.any!(fce => !fce.path.endsWith("parse_tree.html")))
        )
        {
            runTests(args);
        }

        Thread.sleep(500.msecs);
    }
}

void runTests(string[] args)
{
    Pid clearPid = spawnShell("clear");
    wait(clearPid);
    Pid dubTestPid = spawnProcess(["./run_tests.d"] ~ args[1..$]);
    wait(dubTestPid);
}
